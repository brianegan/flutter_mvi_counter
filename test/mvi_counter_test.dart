import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mvi_counter/main.dart';
import 'package:flutter_stream_friends/flutter_stream_friends.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  // Note: The model logic can be tested without any need that a view actually
  // press buttons! We can simulate them easily because they're just Dart
  // Streams :)
  group('Model', () {
    test('it should start with a default of 0', () async {
      expect(new Model(new Stream.empty()), emitsInOrder([0]));
    });

    test('it should increment every time a is received', () async {
      // Since onButtonPressed is really just a Stream<Null>, we can simulate
      // 3 button taps with a Stream of 3 `null` values
      final onButtonPressed = new Stream.fromIterable([null, null, null]);
      final model = new Model(onButtonPressed);

      expect(model, emitsInOrder([0, 1, 2, 3]));
    });
  });

  // The View layer is easy to test because it is simply in charge of
  // taking in the Intent, Model, and Props and turning those into a UI that
  // can be verified with Flutter's test utils.
  group('View', () {
    final counterFinder = find.byKey(View.counterKey);
    final fabFinder = find.byKey(View.fabKey);
    final titleFinder = find.byKey(View.titleKey);

    testWidgets('Binds the passed in title String to the title Widget',
        (WidgetTester tester) async {
      final title = "My Title";
      final intent = new Intent();
      final view = new MaterialApp(
        home: new View(
          title: title,
          intent: intent,
          model: new Model(intent.onButtonPressed),
        ),
      );

      await tester.pumpWidget(view); // Create the widget

      // Expect the Title to be the same as the value we passed through
      expect((tester.firstWidget(titleFinder) as Text).data, title);
    });

    testWidgets(
        'binds the onButtonPressed intent to the Floating action button',
        (WidgetTester tester) async {
      // The power of Streams also shows in this test. Note: We don't need
      // any mocks or special classes, we can simply use the VoidStreamCallback
      // in combination with the test package to verify we've correctly
      // bound the onButtonPressed intent to the floating action button
      final taps = new VoidStreamCallback();
      final intent = new Intent(onButtonPressed: taps);
      final model = new Model(intent.onButtonPressed);
      final view = new MaterialApp(
        home: new View(
          intent: intent,
          model: model,
          title: "Title",
        ),
      );

      // Create the Widget
      await tester.pumpWidget(view);

      // Use the test package to start listening to the taps Stream to ensure it
      // emits 3 times in response to 3 taps on the button below
      expect(taps, emitsInOrder([isNull, isNull, isNull]));

      // Tap the button 3 times
      await tester.tap(fabFinder);
      await tester.tap(fabFinder);
      await tester.tap(fabFinder);
    });

    testWidgets('renders a counter of 0 when no stream data is delivered',
        (WidgetTester tester) async {
      final intent = new Intent();
      final model = new Model(intent.onButtonPressed);
      final view = new MaterialApp(
        home: new View(
          intent: intent,
          model: model,
          title: "Title",
        ),
      );

      await tester.pumpWidget(view); // First render, no Stream data has arrived

      final Text counter = tester.firstWidget(counterFinder);

      expect(counter.data, Model.initialValue.toString());
    });

    testWidgets('Binds the Model state to the counter text',
        (WidgetTester tester) async {
      // Provide a Model with a Single value that we want to test
      final value = 42;
      final intent = new Intent();
      final model = new Stream.fromIterable([value]);
      final view = new MaterialApp(
        home: new View(
          model: model,
          intent: intent,
          title: "My Title",
        ),
      );

      await tester.pumpWidget(view); // Initialize the stream
      await tester.pumpWidget(view); // Receive the provided data

      final Text counterWidget = tester.firstWidget(counterFinder);

      // Expect the View to render the number 42 we sent through
      expect(counterWidget.data, value.toString());
    });
  });
}
