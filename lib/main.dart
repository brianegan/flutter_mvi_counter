import 'dart:async';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_stream_friends/flutter_stream_friends.dart';

void main() {
  runApp(new App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final intent = new Intent();
    final model = new Model(intent.onButtonPressed);

    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new View(
        intent: intent,
        model: model,
        // You can also pass through "normal" props if ya want
        title: 'Flutter Demo Home Page',
      ),
    );
  }
}

// Takes in the View, Intent, and any "normal" props and renders them. There's
// no need for state here, as it is handled by our Model, which delivers the
// last ViewState in response to user or background actions.
class View extends StatelessWidget {
  static const counterKey = const Key('View.counter');
  static const fabKey = const Key('View.fab');
  static const titleKey = const Key('View.title');

  final Intent intent;
  final Stream<int> model;
  final String title;

  View({
    Key key,
    @required this.intent,
    @required this.model,
    @required this.title,
  })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new StreamBuilder(
      stream: model, // Provide the Model as the data source
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        return new Scaffold(
          appBar: new AppBar(
            title: new Text(
              title,
              key: View.titleKey,
            ),
          ),
          body: new Center(
            child: new Card(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    'You have pushed the button this many times:',
                  ),
                  new Text(
                    '${snapshot.hasData ? snapshot.data : Model.initialValue}',
                    key: View.counterKey,
                    style: Theme.of(context).textTheme.display1,
                  )
                ],
              ),
            ),
          ),
          floatingActionButton: new FloatingActionButton(
            key: View.fabKey,
            onPressed: intent.onButtonPressed,
            tooltip: 'Increment',
            child: new Icon(Icons.add),
          ),
        );
      },
    );
  }
}

// Takes in Streams of User Actions and converts them into a proper View State
class Model extends StreamView<int> {
  static const initialValue = 0;

  Model(Stream<Null> onButtonPressed)
      : super(
            // Every time the "button is pressed" (aka the onButtonPressed Stream emits)
            new Observable(onButtonPressed)
                // Emit the value of 1
                .map((_) => 1)
                // Then add 1 to the total sum, which will begin with 0
                .scan((int acc, int val, int i) => acc + val, initialValue)
                // Finally, you can deliver an initial value from the Model
                // to start things off!
                .startWith(initialValue));
}

// Express all the actions our users can take as `StreamCallbacks` in the
// Intent class. StreamCallbacks act as the glue between our View and Model
// layer.
class Intent {
  // Acts as a Stream that can be listened to by the Model when a user presses
  // the Floating Action Button. For it to work, you must set it as the
  // `onPressed` handler on the button.
  final VoidStreamCallback onButtonPressed;

  Intent({VoidStreamCallback onButtonPressed})
      : this.onButtonPressed = onButtonPressed ?? new VoidStreamCallback();
}
